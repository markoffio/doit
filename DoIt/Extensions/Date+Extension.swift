//
//  Date+Extension.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation

extension Date {
    // Convert a date to its string representation
    func dateToRelativeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.doesRelativeDateFormatting = true
        return dateFormatter.string(from: self)
    }
}
