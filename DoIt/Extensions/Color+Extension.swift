//
//  Color+Extension.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation
import UIKit
import SwiftUI

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        var hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if hexString.hasPrefix("#") { hexString.remove(at: hexString.startIndex) }
        var color: UInt64 = 0
        scanner.scanHexInt64(&color)
        
        let mask = 0x000000FF
        let redValue = Int(color >> 16) & mask
        let greenValue = Int(color >> 8) & mask
        let blueValue = Int(color) & mask
        
        let red = CGFloat(redValue) / 255.0
        let green = CGFloat(greenValue) / 255.0
        let blue = CGFloat(blueValue) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    // Convert color to hexadecimal representation
    func hexStringFromColor() -> String {
        var redValue: CGFloat = 0
        var greenValue: CGFloat = 0
        var blueValue: CGFloat = 0
        var alphaValue: CGFloat = 0
        
        getRed(&redValue, green: &greenValue, blue: &blueValue, alpha: &alphaValue)
        
        let rgb: Int = (Int)(redValue * 255) << 16 | (Int)(greenValue * 255) << 8 | (Int)(blueValue * 255) << 0
        
        return String(format: "#%06x", rgb)
    }
}

// Colors
extension Color {
    static let brandPrimary = Color("brandPrimary")
}
