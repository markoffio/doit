//
//  MockGoalRepository.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation

class MockGoalRepository: GoalRepositoryProtocol {
    
    // MARK: - Properties
    // Sample goals
    var goals = Goal.sampleGoals()
    
    // MARK: - Functions
    
    // Fetch all goal
    func fetchAll(completion: @escaping (Result<[Goal]?, Error>) -> Void) {
        completion(.success(goals))
    }
    
    // Add a goal to Firestore database
    func add(goal: Goal, completion: @escaping (Result<Goal?, Error>) -> Void) {
        var goal = goal
        goal.id = UUID().uuidString
        goals.append(goal)
        completion(.success(goal))
    }
    
    // Delete a goal
    func delete(goalID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        goals.removeAll { goal in
            goal.id == goalID
        }
        completion(.success(true))
    }
    
    // Add goal item
    func addGoalItem(goalID: String, item: String, completion: @escaping (Result<Goal?, Error>) -> Void) {
        for i in 0..<goals.count {
            if goals[i].id == goalID {
                goals[i].items.append(item)
                completion(.success(goals[i]))
                return
            }
        }
    }
    
    // Delete goal item
    func deleteGoalItem(goalID: String, item: String, completion: @escaping (Result<Goal?, Error>) -> Void) {
        for i in 0..<goals.count {
            if goals[i].id == goalID {
                goals[i].items.removeAll { item in
                    item == item
                }
                completion(.success(goals[i]))
                return
            }
        }
    }
}
