//
//  FirebaseGoalRepository.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class FirebaseGoalRepository: GoalRepositoryProtocol {
    
    // MARK: - Properties
    // Database object reference
    private let db = Firestore.firestore()
    
    // MARK: - Functions
    
    // Fetch all goal
    func fetchAll(completion: @escaping (Result<[Goal]?, Error>) -> Void) {
        db.collection(Collection.goals).getDocuments { snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                completion(.failure(error ?? NSError(domain: "Snapshot is nill", code: 102, userInfo: nil)))
                return
            }
            let goals: [Goal] = snapshot.documents.compactMap { document in
                var goal = try? document.data(as: Goal.self)
                if goal != nil { goal!.id = document.documentID }
                return goal
            }
            completion(.success(goals))
        }
    }
    
    // Add a goal to Firestore database
    func add(goal: Goal, completion: @escaping(Result<Goal?, Error>) -> Void) {
        do {
            let reference = try db.collection(Collection.goals).addDocument(from: goal)
            reference.getDocument { snapshot, error in
                guard let snapshot = snapshot, error == nil else {
                    completion(.failure(error ?? NSError(domain: "Snapshot is nil", code: 101, userInfo: nil)))
                    return
                }
                // Create goal object
                let goal = try? snapshot.data(as: Goal.self)
                completion(.success(goal))
            }
        } catch let error { completion(.failure(error)) }
    }
    
    // Delete a goal
    func delete(goalID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        db.collection(Collection.goals).document(goalID).delete { error in
            if let error = error { completion(.failure(error)) }
            else { completion(.success(true)) }
        }
    }
    
    // Add goal item
    func addGoalItem(goalID: String, item: String, completion: @escaping (Result<Goal?, Error>) -> Void) {
        let reference = db.collection(Collection.goals).document(goalID)
        reference.updateData(["items" : FieldValue.arrayUnion([item])]) { error in
            if let error = error { completion(.failure(error)) }
            else {
                reference.getDocument { snapshot, error in
                    guard let snapshot = snapshot, error == nil else {
                        completion(.failure(error ?? NSError(domain: "Snapshot is nil", code: 103, userInfo: nil)))
                        return
                    }
                    var goal: Goal? = try? snapshot.data(as: Goal.self)
                    if goal != nil {
                        goal!.id = snapshot.documentID
                        completion(.success(goal))
                    }
                }
            }
        }
    }
    
    // Delete goal item
    func deleteGoalItem(goalID: String, item: String, completion: @escaping (Result<Goal?, Error>) -> Void) {
        let reference = db.collection(Collection.goals).document(goalID)
        reference.updateData(["items" : FieldValue.arrayRemove([item])]) { error in
            if let error = error { completion(.failure(error)) }
            else {
                reference.getDocument { snapshot, error in
                    guard let snapshot = snapshot, error == nil else {
                        completion(.failure(error ?? NSError(domain: "Snapshot is nil", code: 104, userInfo: nil)))
                        return
                    }
                    var goal: Goal? = try? snapshot.data(as: Goal.self)
                    if goal != nil {
                        goal!.id = snapshot.documentID
                        completion(.success(goal))
                    }
                }
            }
        }
    }
}
