//
//  GoalRepositoryProtocol.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation

protocol GoalRepositoryProtocol {
    // Fetch all goal
    func fetchAll(completion: @escaping (Result<[Goal]?, Error>) -> Void)
    // Add a goal to Firestore database
    func add(goal: Goal, completion: @escaping (Result<Goal?, Error>) -> Void)
    // Delete a goal
    func delete(goalID: String, completion: @escaping (Result<Bool, Error>) -> Void)
    // Add goal item
    func addGoalItem(goalID: String, item: String, completion: @escaping (Result<Goal?, Error>) -> Void)
    // Delete goal item
    func deleteGoalItem(goalID: String, item: String, completion: @escaping (Result<Goal?, Error>) -> Void)
}
