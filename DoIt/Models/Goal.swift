//
//  Goal.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation
import SwiftUI
import FirebaseFirestoreSwift

struct Goal: Identifiable, Codable {
    @DocumentID var id: String?
    var name: String
    var dueOn: Date
    var color: String
    var items: [String]
}

extension Goal {
    static func sampleGoals() -> [Goal] {
        return [
            Goal(id: UUID().uuidString, name: "Work", dueOn: Date(), color: "#575CE5", items: [
                "Remove dust",
                "Clean garage"
            ]),
            Goal(id: UUID().uuidString, name: "Study", dueOn: Date(), color: "#575CE5", items: [
                "Learn Python",
                "Learn C#"
            ])
        ]
    }
}

