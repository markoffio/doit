//
//  FirebaseGoalRepository.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import Foundation
import UIKit

enum Collection {
    // Goal
    static let goals: String = "goals"
}
