//
//  Colors+Extension.swift
//  DoIt
//
//  Created by Marco Margarucci on 12/09/21.
//

import Foundation
import SwiftUI

// Colors
extension Color {
    static let brandPrimary = Color("brandPrimary")
}
