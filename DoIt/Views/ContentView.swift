//
//  ContentView.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import SwiftUI

struct ContentView: View {
    // MARK: - Properties
    
    // Repository
    private var repository: GoalRepositoryProtocol
    // View model
    @ObservedObject var goalListViewModel: GoalListViewModel
    // Is editing
    @State private var isEditing: Bool = false
    // Show add new goal
    @State private var showAddNewGoal: Bool = false
    // Goal items
    private var goalItems: [GridItem] {
        Array(repeating: .init(.adaptive(minimum: 120)), count: 2)
    }
    
    init(repository: GoalRepositoryProtocol) {
        self.repository = repository
        goalListViewModel = GoalListViewModel(repository: repository)
        Theme.navigationBarColors(background: .clear, titleColor: .black, tintColor: .white)
    }
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .bottomTrailing) {
                ScrollView(.vertical, showsIndicators: false) {
                    LazyVGrid(columns: goalItems, spacing: 20) {
                        ForEach(goalListViewModel.goals, id: \.id) { goal in
                            NavigationLink(destination: GoalDetailView(viewModel: GoalDetailViewModel(repository: repository, goal: goal))) {
                                GoalView(viewModel: goal)
                                    .blur(radius: isEditing ? 5 : 0)
                                    .disabled(isEditing)
                                    .overlay(deleteButton(goal: goal))
                            }
                        }
                    }
                    .padding(.horizontal)
                }
                .onAppear {
                    goalListViewModel.fetchAllGoals()
                }
                .navigationTitle(goalListViewModel.goals.count != 0 ? "Goals" : "Tap the plus button")
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button {
                            isEditing.toggle()
                        } label: {
                            Text(isEditing ? "DONE" : "EDIT")
                                .font(.custom("AvenirNext-DemiBold", size: 15))
                                .foregroundColor(.brandPrimary)
                        }
                        .opacity(goalListViewModel.goals.count != 0 ? 1.0 : 0.0)
                    }
                }
                
                // Floating button
                Button(action: {
                    showAddNewGoal = true
                }, label: {
                    Image(systemName: "plus")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 24, height: 24)
                        .padding()
                })
                    .background(Color.brandPrimary)
                    .foregroundColor(.white)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    .shadow(color: .gray, radius: 5, x: 2.0, y: 5.0)
                    .padding(.trailing, 20)
                    //.opacity(isEditing ? 0.0 : 1.0)
                    .fullScreenCover(isPresented: $showAddNewGoal) {
                        goalListViewModel.fetchAllGoals()
                    } content: {
                        AddNewGoalView(repository: repository)
                    }

            }
        }
    }
    
    // MARK: - Functions
    
    // Delete button
    private func deleteButton(goal: GoalViewModel) -> some View {
        Button {
            goalListViewModel.deleteGoal(goalID: goal.id)
        } label: {
            Image(systemName: "minus.circle.fill")
        }
        .font(.title)
        .foregroundColor(isEditing ? .white : .clear)
        .disabled(!isEditing)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(repository: MockGoalRepository())
    }
}
