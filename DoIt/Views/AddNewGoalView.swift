//
//  AddNewGoalView.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import SwiftUI

struct AddNewGoalView: View {
    // MARK: - Properties
    
    // Add goal view model
    @StateObject private var addGoalViewModel: AddGoalViewModel
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    
    init(repository: GoalRepositoryProtocol) {
        _addGoalViewModel = StateObject<AddGoalViewModel>.init(wrappedValue: AddGoalViewModel(repository: repository))
    }
    
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                Form {
                    Section {
                        TextField("Goal name", text: $addGoalViewModel.name)
                            .font(.custom("AvenirNext-DemiBold", size: 15))
                        DatePicker(selection: $addGoalViewModel.dueOn, in: Date()..., displayedComponents: .date) {
                            Text("Select goal due date")
                                .font(.custom("AvenirNext-DemiBold", size: 15))
                        }
                        .id(addGoalViewModel.dueOn)
                        
                        /*ColorPicker("Select goal color", selection: $addGoalViewModel.color)
                            .font(.custom("AvenirNext-DemiBold", size: 15))*/
                    }
                }
                .frame(maxHeight: 200)
                List {
                    HStack {
                        TextField("Enter goal", text: $addGoalViewModel.goal)
                            .font(.custom("AvenirNext-DemiBold", size: 15))
                            .onSubmit {
                                addGoalViewModel.addGoalToItemsArray(item: addGoalViewModel.goal)
                            }
                            .submitLabel(.return)
                    }
                    ForEach(addGoalViewModel.items, id: \.self) { item in
                        Text(item)
                            .font(.custom("AvenirNext-DemiBold", size: 15))
                    }
                }
                .listStyle(.insetGrouped)
                .navigationTitle("New goal")
                .toolbar {
                    // Back button
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button {
                            presentationMode.wrappedValue.dismiss()
                        } label: {
                            Image(systemName: "arrow.left")
                                .foregroundColor(.brandPrimary)
                        }
                    }
                    //
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button {
                            addGoalViewModel.add()
                            presentationMode.wrappedValue.dismiss()
                        } label: {
                            Text("SAVE")
                                .font(.custom("AvenirNext-DemiBold", size: 15))
                                .foregroundColor(.brandPrimary)
                        }
                    }
                }
            }
        }
    }
}

struct AddNewGoalView_Previews: PreviewProvider {
    static var previews: some View {
        AddNewGoalView(repository: MockGoalRepository())
    }
}
