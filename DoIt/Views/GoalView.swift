//
//  GoalView.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import SwiftUI

struct GoalView: View {
    // MARK: - Properties
    
    // View model
    let viewModel: GoalViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(viewModel.name)
                .font(.custom("AvenirNext-DemiBold", size: 20))
                .fontWeight(.semibold)
                .multilineTextAlignment(.leading)
            Text(viewModel.dueOn.lowercased())
                .font(.custom("AvenirNext-DemiBold", size: 15))
            HStack(alignment: .bottom) {
                Text(viewModel.items.count > 1 ? "\(viewModel.items.count) items" : "\(viewModel.items.count) item")
                    .font(.custom("AvenirNext-DemiBold", size: 20))
                    .fontWeight(.semibold)
                    .padding(.top, 20)
                Spacer()
            }
        }
        .foregroundColor(.white)
        .padding()
        .background(RoundedRectangle(cornerRadius: 16)
                        .fill(Color.brandPrimary)
                        .shadow(color: .gray, radius: 5, x: 2.0, y: 5.0))
    }
}

struct GoalView_Previews: PreviewProvider {
    static var previews: some View {
        GoalView(viewModel: GoalViewModel(goal: Goal.sampleGoals()[0]))
    }
}
