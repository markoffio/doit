//
//  GoalDetailView.swift
//  DoIt
//
//  Created by Marco Margarucci on 11/10/21.
//

import SwiftUI

struct GoalDetailView: View {
    // MARK: - Properties
    
    // View model
    @ObservedObject var viewModel: GoalDetailViewModel
    
    init(viewModel: GoalDetailViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack(spacing: 0) {
            VStack(alignment: .leading) {
                HStack {
                    Text(viewModel.goal.name)
                        .font(.custom("AvenirNext-DemiBold", size: 20))
                        .bold()
                    Spacer()
                }
                Text("Due \(viewModel.goal.dueOn)")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                    .padding(.vertical)
            }
            .padding(.horizontal)
            .foregroundColor(.white)
            .background(Color.brandPrimary)
            List {
                HStack {
                    TextField("Enter goal", text: $viewModel.item)
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                        .onSubmit { viewModel.add(item: viewModel.item) }
                        .submitLabel(.return)
                }
                ForEach(viewModel.goal.items, id: \.self) { item in
                    Text(item)
                        .font(.custom("AvenirNext-DemiBold", size: 15))
                }
                .onDelete { indexSet in
                    for index in indexSet { viewModel.delete(item: viewModel.goal.items[index]) }
                }
            }
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct GoalDetailView_Previews: PreviewProvider {
    static var previews: some View {
        GoalDetailView(viewModel: GoalDetailViewModel(repository: MockGoalRepository(), goal: GoalViewModel(goal: Goal.sampleGoals()[0])))
    }
}
