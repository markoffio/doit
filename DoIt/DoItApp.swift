//
//  DoItApp.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import SwiftUI
import Firebase

@main
struct DoItApp: App {
    // Firebase configuration
    init() { FirebaseApp.configure() }
    var body: some Scene {
        WindowGroup {
            ContentView(repository: FirebaseGoalRepository())
        }
    }
}
