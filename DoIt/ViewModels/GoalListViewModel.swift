//
//  GoalListViewModel.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation

class GoalListViewModel: ObservableObject {
    // MARK: - Properties
    
    // Goal repository protocol
    private var repository: GoalRepositoryProtocol
    // Goals
    @Published var goals = [GoalViewModel]()
    
    // MARK: - Functions
    
    init(repository: GoalRepositoryProtocol) { self.repository = repository }

    // Fetch all goals
    func fetchAllGoals() {
        repository.fetchAll { result in
            switch result {
            case .success(let fetchedGoals):
                if let fetchedGoals = fetchedGoals {
                    DispatchQueue.main.async {
                        self.goals = fetchedGoals.map(GoalViewModel.init)
                    }
                }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
    
    // Delete a goal
    func deleteGoal(goalID: String) {
        repository.delete(goalID: goalID) { result in
            switch result {
            case .success(let success):
                if success { self.fetchAllGoals() }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
}
