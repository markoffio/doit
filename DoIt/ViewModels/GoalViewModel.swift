//
//  GoalViewModel.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation
import UIKit
import SwiftUI

struct GoalViewModel {
    // MARK: - Properties
    // Goal
    var goal: Goal
    // Id
    var id: String { goal.id ?? "" }
    // Name
    var name: String { goal.name }
    // Due on date
    var dueOnDate: Date { goal.dueOn }
    // Due on
    var dueOn: String { goal.dueOn.dateToRelativeString() }
    // Color
    var color: Color { Color(UIColor(hexString: goal.color)) }
    // Items
    var items: [String] {
        get { return goal.items }
        set(newItems) { goal.items = newItems }
    }
}
