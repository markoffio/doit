//
//  AddGoalViewModel.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation
import SwiftUI

class AddGoalViewModel: ObservableObject {
    // MARK: - Properties
    
    // Repository
    private let repository: GoalRepositoryProtocol
    
    // Saved
    @Published var saved: Bool = false
    // Items array
    @Published var items: [String] = []
    
    // Name
    var name: String = ""
    // Due on
    var dueOn = Date()
    // Color
    var color: Color = .brandPrimary
    // Goal
    var goal: String = ""
    
    // MARK: - Functions

    init(repository: GoalRepositoryProtocol) {
        self.repository = repository
    }
    
    // Add goal to item array
    func addGoalToItemsArray(item: String) {
        guard !item.isEmpty, item.count >= 3 else { return }
        items.append(item)
        goal = ""
    }
    
    // Add a goal to the repository
    func add() {
        let goal = Goal(name: name, dueOn: dueOn, color: UIColor(color).hexStringFromColor(), items: items)
        repository.add(goal: goal) { result in
            switch result {
            case .success(let savedGoal):
                DispatchQueue.main.async { self.saved = savedGoal == nil ? false : true }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
}

