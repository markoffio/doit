//
//  GoalDetailViewModel.swift
//  DoIt
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation
import SwiftUI

class GoalDetailViewModel: ObservableObject {
    // MARK: - Properties
    
    // Repository
    private let repository: GoalRepositoryProtocol
    
    // Goal view model
    @Published var goal: GoalViewModel
    // Item
    var item: String = ""
    
    // MARK: Functions
    
    init(repository: GoalRepositoryProtocol, goal: GoalViewModel) {
        self.repository = repository
        self.goal = goal
    }
    
    // Add an item
    func add(item: String) {
        repository.addGoalItem(goalID: goal.id, item: item) { result in
            switch result {
            case .success(let newGoal):
                if let newGoal = newGoal {
                    DispatchQueue.main.async {
                        self.goal = GoalViewModel(goal: newGoal)
                        self.item = ""
                    }
                }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
    
    // Delete an item
    func delete(item: String) {
        repository.deleteGoalItem(goalID: goal.id, item: item) { result in
            switch result {
            case .success(let newGoal):
                if let newGoal = newGoal {
                    DispatchQueue.main.async {
                        self.goal = GoalViewModel(goal: newGoal)
                    }
                }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
}
